<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::middleware(['auth'])->group(function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('classrooms', 'ClassroomsController');
    Route::resource('courses', 'CoursesController');
    Route::resource('departments', 'DepartmentsController');
    Route::resource('sections', 'SectionsController');
    Route::resource('students', 'StudentsController');
    Route::resource('teachers', 'TeachersController');
    Route::resource('times', 'TimesController');
    Route::resource('users', 'UsersController');
});

Route::middleware(['auth'])->group(function () {
    Route::get('exportTeachers', 'TeachersController@export')->name('teachers.export');
    Route::post('importTeachers', 'TeachersController@import')->name('teachers.import');
    Route::get('exportStudents', 'StudentsController@export')->name('students.export');
    Route::post('importStudents', 'StudentsController@import')->name('students.import');
    Route::get('exportClassrooms', 'ClassroomsController@export')->name('classrooms.export');
    Route::post('importClassrooms', 'ClassroomsController@import')->name('classrooms.import');
});