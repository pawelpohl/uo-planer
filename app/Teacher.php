<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $fillable = [
        'Name',
        'Surname',
        'Email',
        'PhoneNumber',
        'DateOfBirth',
        'Pesel',
        'TeacherID',
        'Degree',       
    ];

     /* //wiele kursow, wiele czasu
     public function courses()
     {
         return $this->belongsToMany(Course::class);
     }
 
     public function times()
     {
         return $this->belongsToMany(Time::class);
     }

     public function departments()
     {
         return $this->belongsTo(Department::class);
     } */
}
