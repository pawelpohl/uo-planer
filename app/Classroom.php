<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classroom extends Model
{
    protected $fillable = [
        'ClassroomNo',
        'Description',
        'Cpacity',
        'Laboratory',
        'Conversatory',
        'Lecture',
        'Seminar',   
    ];

      //wiele kursow, wiele czasu
     public function courses()
     {
         return $this->belongsToMany(Course::class);
     }
 /*
     public function times()
     {
         return $this->belongsToMany(Time::class);
     }

     public function departments()
     {
         return $this->belongsTo(Department::class);
     } */
}
