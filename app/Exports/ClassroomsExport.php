<?php

namespace App\Exports;

use App\Classroom;
use Maatwebsite\Excel\Concerns\FromCollection;

class ClassroomsExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Classroom::all();
    }
}