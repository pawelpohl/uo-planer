<?php

namespace App\Imports;

use App\Teacher;
use Maatwebsite\Excel\Concerns\ToModel;

class TeachersImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Teacher([
            'Name'  => $row[1],
            'Surname' => $row[2], 
            'Email' => $row[3],
            'PhoneNumber' => $row[4],
            'DateOfBirth' => $row[5],
            'Pesel' => $row[6],
            'TeacherID' => $row[7],
            'Degree' => $row[8],
        ]);
    }
}
