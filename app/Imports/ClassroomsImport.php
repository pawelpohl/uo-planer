<?php

namespace App\Imports;

use App\Classroom;
use Maatwebsite\Excel\Concerns\ToModel;

class TeachersImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Classroom([
            'ClassroomNo'  => $row[1],
            'Description' => $row[2], 
            'Capacity' => $row[3],
            'Laboratory' => $row[4],
            'Conversatory' => $row[5],
            'Lecture' => $row[6],
            'Seminar' => $row[7],
        ]);
    }
}
