<?php

namespace App\Imports;

use App\Student;
use Maatwebsite\Excel\Concerns\ToModel;

class StudentsImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Student([
            'Name'  => $row[1],
            'Surname' => $row[2], 
            'Email' => $row[3],
            'PhoneNumber' => $row[4],
            'DateOfBirth' => $row[5],
            'Pesel' => $row[6],
            'IndexNumber' => $row[7],
            'IsActive' => $row[8],
            'DegreeLevel' => $row[9],
            'DegreeType' =>   $row[10],
        ]);
    }
}
