<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CreateStudentRequest;
use App\Http\Requests\UpdateStudentRequest;
use App\Student;
use App\Exports\StudentsExport;
use App\Imports\StudentsImport;
use Maatwebsite\Excel\Facades\Excel;

class StudentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('students.index')->with('students', Student::paginate(4));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('students.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateStudentRequest $request)
    {
           $student = Student::create([
                'Name' => $request->Name,
                'Surname' => $request->Surname,
                'Email' => $request->Email,
                'PhoneNumber' => $request->PhoneNumber,
                'DateOfBirth' => $request->DateOfBirth,
                'Pesel' => $request->Pesel,
                'IndexNumber' => $request->IndexNumber,
                'IsActive' => $request->IsActive,
                'DegreeLevel' => $request->DegreeLevel,
                'DegreeType' => $request->DegreeType
           ]);
           // flash message
           session()->flash('success', 'Student added successfully.');
           // redirect user
   
           return redirect(route('students.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        return view('students.create')->with('student', $student);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateStudentRequest $request, Student $student)
    {
        $student->update([
            'Name' => $request->Name,
            'Surname' => $request->Surname,
            'Email' => $request->Email,
            'PhoneNumber' => $request->PhoneNumber,
            'DateOfBirth' => $request->DateOfBirth,
            'Pesel' => $request->Pesel,
            'IndexNumber' => $request->IndexNumber,
            'IsActive' => $request->IsActive,
            'DegreeLevel' => $request->DegreeLevel,
            'DegreeType' => $request->DegreeType
        ]);
          // flash message
          session()->flash('success', 'Student updated successfully.');
          // redirect user
  
          return redirect(route('students.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $student = Student::where('id', $id)->firstOrFail();
        $student->delete();
        session()->flash('success', 'Student deleted successfully.');

        return redirect(route('students.index'));
    }
    /**
     * Export students to csv file
     *
     */

     public function export()
     {
        return Excel::download(new StudentsExport, 'students.csv');
     }  
     
     /**
     * Import csv file with students
     *
     */
     public function import() 
     {
         Excel::import(new StudentsImport,request()->file('file'));
            
         return back();
     }
}
