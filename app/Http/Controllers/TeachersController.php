<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Teacher;
use App\Exports\TeachersExport;
use App\Imports\TeachersImport;
use App\Http\Requests\CreateTeacherRequest;
use App\Http\Requests\UpdateTeacherRequest;
use Maatwebsite\Excel\Facades\Excel;

class TeachersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('teachers.index')->with('teachers', Teacher::paginate(4));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('teachers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTeacherRequest $request)
    {
        $teacher = Teacher::create([
            'Name' => $request->Name,
            'Surname' => $request->Surname,
            'Email' => $request->Email,
            'PhoneNumber' => $request->PhoneNumber,
            'DateOfBirth' => $request->DateOfBirth,
            'Pesel' => $request->Pesel,
            'TeacherID' => $request->TeacherID,
            'Degree' => $request->Degree
       ]);
       // flash message
       session()->flash('success', 'Teacher added successfully.');
       // redirect user

       return redirect(route('teachers.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Teacher $teacher)
    {
        return view('teachers.create')->with('teacher', $teacher);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTeacherRequest $request, Teacher $teacher)
    {
        $teacher->update([
            'Name' => $request->Name,
            'Surname' => $request->Surname,
            'Email' => $request->Email,
            'PhoneNumber' => $request->PhoneNumber,
            'DateOfBirth' => $request->DateOfBirth,
            'Pesel' => $request->Pesel,
            'TeacherID' => $request->TeacherID,
            'Degree' => $request->Degree
       ]);
       // flash message
       session()->flash('success', 'Teacher updated successfully.');
       // redirect user

       return redirect(route('teachers.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $teacher = Teacher::where('id', $id)->firstOrFail();
        $teacher->delete();
        session()->flash('success', 'Teacher deleted successfully.');

        return redirect(route('teachers.index'));
    }

    /**
     * Export students to csv file
     *
     */

    public function export()
     {
        return Excel::download(new TeachersExport, 'teachers.csv');
     } 

     /**
     * Import csv file with students
     *
     */

     public function import() 
     {
         Excel::import(new TeachersImport,request()->file('file'));
            
         return back();
     }
}
