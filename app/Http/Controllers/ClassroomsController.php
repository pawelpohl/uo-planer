<?php

namespace App\Http\Controllers;

use App\Classroom;
use App\Exports\ClassrooomsExport;
use App\Imports\ClassrooomsImport;
use Illuminate\Http\Request;
use App\Http\Requests\CreateClassroomRequest;
use App\Http\Requests\UpdateClassroomRequest;

use Maatwebsite\Excel\Facades\Excel;

class ClassroomsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('classrooms.index')->with('classrooms', Classroom::paginate(4));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('classrooms.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateClassroomRequest $request)
    {
        $classroom = Classroom::create([
            'ClassroomNo' => $request->ClassroomNo,
            'Desctiption' => $request->Desctiption,
            'Capacity' => $request->Capacity,
            'Laboratory' => $request->Laboratory,
            'Conversatory' => $request->Conversatory,
            'Lecture' => $request->Lecture,
            'Seminar' => $request->Seminar,   
       ]);
       // flash message
       session()->flash('success', 'Classroom added successfully.');
       // redirect user

       return redirect(route('classrooms.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Classroom $classroom)
    {
        return view('classrooms.create')->with('classroom', $classroom);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateClassroomRequest $request, Classroom $classroom)
    {
        $classroom->update([
            'ClassroomNo' => $request->ClassroomNo,
            'Desctiption' => $request->Desctiption,
            'Capacity' => $request->Capacity,
            'Laboratory' => $request->Laboratory,
            'Conversatory' => $request->Conversatory,
            'Lecture' => $request->Lecture,
            'Seminar' => $request->Seminar,   
       ]);
       // flash message
       session()->flash('success', 'Classroom updated successfully.');
       // redirect user

       return redirect(route('classrooms.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $classroom = Classroom::where('id', $id)->firstOrFail();
        $classroom->delete();
        session()->flash('success', 'Classroom deleted successfully.');

        return redirect(route('classrooms.index'));
    }

    /**
     * Export students to csv file
     *
     */

    public function export()
     {
        return Excel::download(new ClassroomsExport, 'classroom.csv');
     } 

     /**
     * Import csv file with students
     *
     */

     public function import() 
     {
         Excel::import(new ClassroomsImport,request()->file('file'));
            
         return back();
     }
}
