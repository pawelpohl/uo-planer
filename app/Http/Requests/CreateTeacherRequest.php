<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateTeacherRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'Name' => 'required|alpha',
            'Surname' => 'required|alpha',
            'Email' => 'required|unique:teachers',
            'PhoneNumber' => 'required|digits:9|unique:teachers',
            'DateOfBirth' => 'required|date',
            'Pesel' => 'required|digits:11|unique:teachers',
            'TeacherID' => 'required|digits:6|unique:teachers',
            'Degree' => 'required'
        ];
    }
}
