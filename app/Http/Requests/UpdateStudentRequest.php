<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateStudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'Name' => 'required|alpha',
            'Surname' => 'required|alpha',
            'Email' => 'required|unique:students,Email,' . $this->student->id,
            'PhoneNumber' => 'required|digits:9|unique:students,PhoneNumber,' . $this->student->id,
            'DateOfBirth' => 'required|date',
            'Pesel' => 'required|digits:11|unique:students,Pesel,' . $this->student->id,
            'IndexNumber' => 'required|digits:6|unique:students,IndexNumber,' . $this->student->id,
            'IsActive' => 'required',
            'DegreeLevel' => 'required',
            'DegreeType'=>'required'
        ];
    }
}
