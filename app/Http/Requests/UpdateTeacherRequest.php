<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTeacherRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'Name' => 'required|alpha',
            'Surname' => 'required|alpha',
            'Email' => 'required|unique:teachers,Email,' . $this->teacher->id,
            'PhoneNumber' => 'required|digits:9|unique:teachers,PhoneNumber,' . $this->teacher->id,
            'DateOfBirth' => 'required|date',
            'Pesel' => 'required|digits:11|unique:teachers,Pesel,' . $this->teacher->id,
            'TeacherID' => 'required|digits:6|unique:teachers,TeacherID,' . $this->teacher->id,
            'Degree' => 'required'
        ];
    }
}
