<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;


class CreateStudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'Name' => 'required|alpha',
            'Surname' => 'required|alpha',
            'Email' => 'required|unique:students',
            'PhoneNumber' => 'required|digits:9|unique:students',
            'DateOfBirth' => 'required|date',
            'Pesel' => 'required|digits:11|unique:students',
            'IndexNumber' => 'required|digits:6|unique:students',
            'IsActive' => 'required',
            'DegreeLevel' => 'required',
            'DegreeType'=>'required'
        ];
    }
}
