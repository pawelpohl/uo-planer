<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [
        'Name',
        'Surname',
        'Email',
        'PhoneNumber',
        'IndexNumber',
        'DateOfBirth',
        'Pesel',
        'IsActive',
        'DegreeLevel',
        'DegreeType',       
    ];

    //wiele kursow, wiele section
    public function courses()
    {
        return $this->belongsToMany(Course::class);
    }

    public function sections()
    {
        return $this->belongsToMany(Section::class);
    }

}
