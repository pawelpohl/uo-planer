@extends('layouts.app')

@section('content')

  <div class="d-flex justify-content-end mb-2">
  <a href="{{ route('classrooms.create') }}" class="btn btn-secondary btn-lg">Add Classroom</a>
  </div>

  <div class="d-flex justify-content-end mb-5">
  <a href="{{ route('classrooms.export') }}" class="btn btn-secondary btn-lg">Export to csv</a>
  </div>
  <div class="d-flex justify-content-end mb-2">
  <form action="{{ route('classrooms.import') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <input type="file" name="file" class="form-control d-flex justify-content-end">
    <button class="btn btn-secondary btn-lg">Import Classroom</button>
  </div>

</div>


  <div class="card card-default">
    <div class="card-header">Classroom List</div>
  

  <div class="card-body">
     @include('partials.errors')
    @if($classrooms->count() > 0)
      <table class="table text-white">
        <thead>
          <th>ClassroomNo</th>
          <th>Description</th>
          <th>Capacity</th>
          <th>Laboratory</th>
          <th>Conversatory</th>
          <th>Lecture</th>
          <th>Seminar</th>
          <th></th>
          <th></th>
        </thead>
        <tbody>
          @foreach($classrooms as $classroom)
            <tr>
              <td>
                    {{-- name --}}
                    {{ $classroom->ClassroomNo }}
              </td>
              <td>
                {{-- Surname --}}
                {{ $classroom->Description }}
              </td>
               <td>
                {{-- Email --}}
                {{ $classroom->Capacity }}
              </td>
              <td>
               {{-- PhoneNumber --}}
               {{ $classroom->Laboratory }}
             </td>
             <td>
               {{-- DateOfBNirth --}}
               {{ $classroom->Conversatory }}
             </td>
             <td>
               {{-- Pesel --}}
               {{ $classroom->Lecture }}
             </td>
             <td>
              {{-- Pesel --}}
              {{ $classroom->Seminar }}
            </td>
             <td>
               <a href="{{ route('classrooms.edit', $classroom->id) }}" class="btn btn-secondary btn-sm">Edit</a>
             </td>
              <td>
              <form action="{{ route('classrooms.destroy', $classroom->id) }}" method="POST">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-secondary btn-sm">
                    Delete
                </button>
              </form>
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
      {{$classrooms->links()}}
    @else
      <h3 class="text-center">No Classrooms Yet</h3>
    @endif
  
</div>

@endsection
@section('scripts')

@endsection

@section('css')

@endsection