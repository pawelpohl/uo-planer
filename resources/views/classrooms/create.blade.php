@extends('layouts.app')

@section('content')
<div class="card card-default">
  <div class="card-header">
    {{ isset($classroom) ? 'Edit Classroom': 'Add Classroom' }}
  </div>

  <div class="card-body">
    @include('partials.errors')
    <form action="{{ isset($classroom) ? route('classrooms.update', $classroom->id) : route('classrooms.store') }}" method="POST">
      @csrf

      @if(isset($classroom))
        @method('PUT')
      @endif

      <div class="form-group">
        <label for="ClassroomNo">ClassroomNo</label>
        <input type="text" class="form-control" name="ClassroomNo" id='ClassroomNo' value="{{ isset($classroom) ? $classroom->ClassroomNo: '' }}">
      </div>
      <div class="form-group">
        <label for="Description">Description</label>
        <input type="text" class="form-control" name="Description" id='Description' value="{{ isset($classroom) ? $classroom->Description: '' }}">
      </div>
      <div class="form-group"> 
           {{-- Czy email ma jakas controlke? --}}
        <label for="Capacity">Capacity</label> 
        <input type="text" class="form-control" name="Capacity" id='Capacity' value="{{ isset($classroom) ? $classroom->Capacity: '' }}">
      </div>
      <div class="form-group">
           {{-- Tylko cyfry i max dlugosc --}}
        <label for="Laboratory">Laboratory</label>
        <input type="number" class="form-control" name="Laboratory" id='Laboratory' value="{{ isset($classroom) ? $classroom->Laboratory: '' }}">
      </div>
      <div class="form-group">
              {{-- wybieraczka jakas tam  --}}
          <label for="Conversatory">Conversatory</label>
          <input type="text" class="form-control" name="Conversatory" id='Conversatory' value="{{ isset($classroom) ? $classroom->Conversatory : '' }}">
     </div>
     <div class="form-group">
           {{-- tylko liczby i max dlugosc --}}
        <label for="Lecture">Lecture</label>
        <input type="number" class="form-control" name="Lecture" id='Lecture' value="{{ isset($classroom) ? $classroom->Lecture: '' }}">
      </div> 
      <div class="form-group">
        <label for="Seminar">Seminar</label>
        <input type="number" class="form-control" name="Seminar" id='Seminar' value="{{ isset($classroom) ? $classroom->Seminar: '' }}">
      </div>
    
      <div class="form-group">
        <a href="{{route('classrooms.index')}}" class="btn btn-secondary">Back</a>
        <button type="submit"  class="btn btn-secondary btn-lg">
          {{ isset($classroom) ? 'Update Classroom': 'Add Classroom' }}
        </button>        
      </div>
    </form>
  </div>
</div>
@endsection

@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

@endsection

@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
@endsection