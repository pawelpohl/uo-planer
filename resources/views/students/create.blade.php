@extends('layouts.app')

@section('content')
<div class="card card-default">
  <div class="card-header">
    {{ isset($student) ? 'Edit Student': 'Add Student' }}
  </div>

  <div class="card-body">
    @include('partials.errors')
    <form action="{{ isset($student) ? route('students.update', $student->id) : route('students.store') }}" method="POST">
      @csrf

      @if(isset($student))
        @method('PUT')
      @endif

      <div class="form-group">
        <label for="Name">Name</label>
        <input type="text" class="form-control" name="Name" id='Name' value="{{ isset($student) ? $student->Name: '' }}">
      </div>
      <div class="form-group">
        <label for="Surname">Surname</label>
        <input type="text" class="form-control" name="Surname" id='Surname' value="{{ isset($student) ? $student->Surname: '' }}">
      </div>
      <div class="form-group">
          <label for="IndexNumber">IndexNumber</label>
          <input type="number" class="form-control" name="IndexNumber" id='IndexNumber' value="{{ isset($student) ? $student->IndexNumber: '' }}">
        </div>
      <div class="form-group"> 
           {{-- Czy email ma jakas controlke? --}}
        <label for="Email">Email</label> 
        <input type="text" class="form-control" name="Email" id='Email' value="{{ isset($student) ? $student->Email: '' }}">
      </div>
      <div class="form-group">
           {{-- Tylko cyfry i max dlugosc --}}
        <label for="PhoneNumber">PhoneNumber</label>
        <input type="number" class="form-control" name="PhoneNumber" id='PhoneNumber' value="{{ isset($student) ? $student->PhoneNumber: '' }}">
      </div>
      <div class="form-group">
              {{-- wybieraczka jakas tam  --}}
          <label for="DateOfBirth">DateOfBirth</label>
          <input type="text" class="form-control" name="DateOfBirth" id='DateOfBirth' value="{{ isset($student) ? $student->DateOfBirth : '' }}">
     </div>
     <div class="form-group">
           {{-- tylko liczby i max dlugosc --}}
        <label for="Pesel">Pesel</label>
        <input type="number" class="form-control" name="Pesel" id='Pesel' value="{{ isset($student) ? $student->Pesel: '' }}">
      </div>      
      <div class="form-group">
        <label for="IsActive">IsActive</label>
        @if(isset($student))
          <select name="IsActive" id="IsActive" class="form-control">                      
               <option value="0" @if($student->DegreeLevel == 0) selected @endif>Nie aktywny</option>
               <option value="1" @if($student->DegreeLevel == 1) selected @endif>Aktywny</option>          
          </select>
        @else
          <select name="IsActive" id="IsActive" class="form-control">        
               <option value="">Wybierz</option>
               <option value="0">Nie aktywny</option>
               <option value="1">Aktywny</option>          
          </select>
        @endif
      </div>
      <div class="form-group">
          <label for="DegreeLevel">DegreeLevel</label>
          @if (isset($student))
          <select name="DegreeLevel" id="DegreeLevel" class="form-control">        
               <option value="1" @if($student->DegreeLevel == 1) selected @endif>Pierwszy stopień</option>
               <option value="2" @if($student->DegreeLevel == 2) selected @endif>Drugi stopień</option>          
               <option value="3" @if($student->DegreeLevel == 3) selected @endif>Trzeci stopień</option>          
          </select>
          @else
          <select name="DegreeLevel" id="DegreeLevel" class="form-control">        
               <option value="0">Wybierz</option>
               <option value="1">Pierwszy stopień</option>
               <option value="2">Drugi stopień</option>          
               <option value="3">Trzeci stopień</option>          
          </select>
          @endif
     </div>
      <div class="form-group">
        <label for="DegreeType">DegreeType</label>
          <select name="DegreeType" id="DegreeType" class="form-control"> 
          @if (isset($student))
               <option value="inżynierskie" @if($student->DegreeType == "inżynierskie") selected @endif >inżynierskie</option>
               <option value="licencjackie" @if($student->DegreeType == "licencjackie") selected @endif>licencjackie</option>          
               <option value="magisterskie" @if($student->DegreeType == "magisterskie") selected @endif>magisterskie</option>          
               <option value="doktoranckie" @if($student->DegreeType == "doktoranckie") selected @endif>doktoranckie</option>  
          @else
               <option value="" >Wybierz</option>
               <option value="inżynierskie">inżynierskie</option>
               <option value="licencjackie">licencjackie</option>          
               <option value="magisterskie">magisterskie</option>          
               <option value="doktoranckie">doktoranckie</option>  
          @endif               
          </select>
      </div>
      <div class="form-group">
        <a href="{{route('students.index')}}" class="btn btn-secondary">Back</a>
        <button type="submit"  class="btn btn-secondary btn-lg">
          {{ isset($student) ? 'Update Student': 'Add Student' }}
        </button>        
      </div>
    </form>
  </div>
</div>
@endsection

@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
     flatpickr('#DateOfBirth', {
       enableTime: false,
       enableSeconds: false
     })
</script>
@endsection

@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
@endsection
