@extends('layouts.app')

@section('content')

  <div class="d-flex justify-content-end mb-2">
  <a href="{{ route('students.create') }}" class="btn btn-secondary btn-lg">Add Student</a>
  </div>

  <div class="d-flex justify-content-end mb-5">
  <a href="{{ route('students.export') }}" class="btn btn-secondary btn-lg">Export to csv</a>
  </div>
  <div class="d-flex justify-content-end mb-2">
  <form action="{{ route('students.import') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <input type="file" name="file" class="form-control d-flex justify-content-end">
    <button class="btn btn-secondary btn-lg">Import Students</button>
  </div>

</div>


  <div class="card card-default">
    <div class="card-header">Students List</div>

  <div class="card-body">
     @include('partials.errors')
    @if($students->count() > 0)
      <table class="table text-white">
        <thead>
          <th>Name</th>
          <th>Surname</th>
          <th>Index</th>
          <th>Email</th>
          <th>PhoneNumber</th>
          <th>DateOfBirth</th>
          <th>Pesel</th>
          <th>IsActive</th>
          <th>DegreeLevel</th>
          <th>DegreeType</th>
          <th></th>
          <th></th>
        </thead>
        <tbody>
          @foreach($students as $student)
            <tr>
              <td>
                    {{-- name --}}
                    {{ $student->Name }}
              </td>
              <td>
                {{-- Surname --}}
                {{ $student->Surname }}
              </td>
              <td>
                {{-- Surname --}}
                {{ $student->IndexNumber }}
              </td>
               <td>
                {{-- Email --}}
                {{ $student->Email }}
              </td>
              <td>
               {{-- PhoneNumber --}}
               {{ $student->PhoneNumber }}
             </td>
             <td>
               {{-- DateOfBNirth --}}
               {{ $student->DateOfBirth }}
             </td>
             <td>
               {{-- Pesel --}}
               {{ $student->Pesel }}
             </td>
             <td>
               {{-- IsActive --}}
              @if($student->IsActive)
                  Aktywny
              @else
                  Nie Aktywny
              @endif
             </td>
             <td>
               @if($student->DegreeLevel == 1)
               Pierwszy stopień
           @else
               Drugi stopień
           @endif
             </td>
             <td>
               {{-- DegreeType --}}
                {{$student->DegreeType}}
             </td>
             <td>
               <a href="{{ route('students.edit', $student->id) }}" class="btn btn-secondary btn-sm">Edit</a>
             </td>
              <td>
              <form action="{{ route('students.destroy', $student->id) }}" method="POST">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-secondary btn-sm">
                    Delete
                </button>
              </form>
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
      {{$students->links()}}
    @else
      <h3 class="text-center">No Students Yet</h3>
    @endif
  
</div>
@endsection
@section('scripts')

@endsection

@section('css')

@endsection
