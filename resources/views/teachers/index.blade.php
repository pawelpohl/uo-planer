@extends('layouts.app')

@section('content')

  <div class="d-flex justify-content-end mb-2">
  <a href="{{ route('teachers.create') }}" class="btn btn-secondary btn-lg">Add Teacher</a>
  </div>

  <div class="d-flex justify-content-end mb-5">
  <a href="{{ route('teachers.export') }}" class="btn btn-secondary btn-lg">Export to csv</a>
  </div>
  <div class="d-flex justify-content-end mb-2">
  <form action="{{ route('teachers.import') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <input type="file" name="file" class="form-control d-flex justify-content-end">
    <button class="btn btn-secondary btn-lg">Import Teachers</button>
  </div>

</div>


  <div class="card card-default">
    <div class="card-header">Teachers List</div>
  

  <div class="card-body">
     @include('partials.errors')
    @if($teachers->count() > 0)
      <table class="table text-white">
        <thead>
          <th>Name</th>
          <th>Surname</th>
          <th>Email</th>
          <th>PhoneNumber</th>
          <th>DateOfBirth</th>
          <th>Pesel</th>
          <th>TeacherID</th>
          <th>Degree</th>
          <th></th>
          <th></th>
        </thead>
        <tbody>
          @foreach($teachers as $teacher)
            <tr>
              <td>
                    {{-- name --}}
                    {{ $teacher->Name }}
              </td>
              <td>
                {{-- Surname --}}
                {{ $teacher->Surname }}
              </td>
               <td>
                {{-- Email --}}
                {{ $teacher->Email }}
              </td>
              <td>
               {{-- PhoneNumber --}}
               {{ $teacher->PhoneNumber }}
             </td>
             <td>
               {{-- DateOfBNirth --}}
               {{ $teacher->DateOfBirth }}
             </td>
             <td>
               {{-- Pesel --}}
               {{ $teacher->Pesel }}
             </td>
             <td>
              {{-- Pesel --}}
              {{ $teacher->TeacherID }}
            </td>
             <td>
               {{-- DegreeType --}}
                {{$teacher->Degree}}
             </td>
             <td>
               <a href="{{ route('teachers.edit', $teacher->id) }}" class="btn btn-secondary btn-sm">Edit</a>
             </td>
              <td>
              <form action="{{ route('teachers.destroy', $teacher->id) }}" method="POST">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-secondary btn-sm">
                    Delete
                </button>
              </form>
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
      {{$teachers->links()}}
    @else
      <h3 class="text-center">No Teachers Yet</h3>
    @endif
  
</div>

@endsection
@section('scripts')

@endsection

@section('css')

@endsection