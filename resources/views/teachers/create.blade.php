@extends('layouts.app')

@section('content')
<div class="card card-default">
  <div class="card-header">
    {{ isset($teacher) ? 'Edit Teacher': 'Add Teacher' }}
  </div>

  <div class="card-body">
    @include('partials.errors')
    <form action="{{ isset($teacher) ? route('teachers.update', $teacher->id) : route('teachers.store') }}" method="POST">
      @csrf

      @if(isset($teacher))
        @method('PUT')
      @endif

      <div class="form-group">
        <label for="Name">Name</label>
        <input type="text" class="form-control" name="Name" id='Name' value="{{ isset($teacher) ? $teacher->Name: '' }}">
      </div>
      <div class="form-group">
        <label for="Surname">Surname</label>
        <input type="text" class="form-control" name="Surname" id='Surname' value="{{ isset($teacher) ? $teacher->Surname: '' }}">
      </div>
      <div class="form-group"> 
           {{-- Czy email ma jakas controlke? --}}
        <label for="Email">Email</label> 
        <input type="text" class="form-control" name="Email" id='Email' value="{{ isset($teacher) ? $teacher->Email: '' }}">
      </div>
      <div class="form-group">
           {{-- Tylko cyfry i max dlugosc --}}
        <label for="PhoneNumber">PhoneNumber</label>
        <input type="number" class="form-control" name="PhoneNumber" id='PhoneNumber' value="{{ isset($teacher) ? $teacher->PhoneNumber: '' }}">
      </div>
      <div class="form-group">
              {{-- wybieraczka jakas tam  --}}
          <label for="DateOfBirth">DateOfBirth</label>
          <input type="text" class="form-control" name="DateOfBirth" id='DateOfBirth' value="{{ isset($teacher) ? $teacher->DateOfBirth : '' }}">
     </div>
     <div class="form-group">
           {{-- tylko liczby i max dlugosc --}}
        <label for="Pesel">Pesel</label>
        <input type="number" class="form-control" name="Pesel" id='Pesel' value="{{ isset($teacher) ? $teacher->Pesel: '' }}">
      </div> 
      <div class="form-group">
        <label for="TeacherID">TeacherID</label>
        <input type="number" class="form-control" name="TeacherID" id='TeacherID' value="{{ isset($teacher) ? $teacher->TeacherID: '' }}">
      </div>
    <div class="form-group">      
      <div class="form-group">
          <label for="Degree">Degree</label>
          @if (isset($teacher))
          <select name="Degree" id="Degree" class="form-control">        
               <option value="licencjat" @if($teacher->Degree == "licencjat") selected @endif>licencjat</option>
               <option value="inżynier" @if($teacher->Degree == "inżynier") selected @endif>inżynier</option>
               <option value="magister" @if($teacher->Degree == "magister") selected @endif>magister</option>          
               <option value="doktor" @if($teacher->Degree == "doktor") selected @endif>doktor</option>     
               <option value="doktor habilitwany" @if($teacher->Degree == "doktor habilitwany") selected @endif>doktor habilitwany</option>
               <option value="profesor" @if($teacher->Degree == "profesor") selected @endif>profesor</option>        
          </select>
          @else
          <select name="Degree" id="Degree" class="form-control">        
               <option value="">Wybierz</option> 
               <option value="licencjat">licencjat</option>
               <option value="inżynier">inżynier</option>
               <option value="magister">magister</option>          
               <option value="doktor">doktor</option>     
               <option value="doktor habilitwany">doktor habilitwany</option>
               <option value="profesor">profesor</option>        
          </select>
          @endif
     </div>
      <div class="form-group">
        <a href="{{route('teachers.index')}}" class="btn btn-secondary">Back</a>
        <button type="submit"  class="btn btn-secondary btn-lg">
          {{ isset($teacher) ? 'Update Teacher': 'Add Teacher' }}
        </button>        
      </div>
    </form>
  </div>
</div>
@endsection

@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
     flatpickr('#DateOfBirth', {
       enableTime: false,
       enableSeconds: false
     })
</script>
@endsection

@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
@endsection