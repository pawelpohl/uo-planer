<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class SectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sections')->insert([[
            'Semester'=>'1',
            'Year'=>'1',
            'Group'=>'1'
            
            
        ],[
            'Semester'=>'1',
            'Year'=>'1',
            'Group'=>'2'
            
        ],[
            'Semester'=>'1',
            'Year'=>'1',
            'Group'=>'3'
            
        ],[
            'Semester'=>'2',
            'Year'=>'1',
            'Group'=>'1'
            
        ],[
            'Semester'=>'2',
            'Year'=>'1',
            'Group'=>'2'
            
        ],[
            'Semester'=>'2',
            'Year'=>'1',
            'Group'=>'3'
            
        ],[
            'Semester'=>'3',
            'Year'=>'2',
            'Group'=>'1'
            
        ],[
            'Semester'=>'3',
            'Year'=>'2',
            'Group'=>'2'
            
        ],[
            'Semester'=>'4',
            'Year'=>'2',
            'Group'=>'1'
            
        ],[
            'Semester'=>'4',
            'Year'=>'2',
            'Group'=>'2'
            
        ],[
            'Semester'=>'5',
            'Year'=>'3',
            'Group'=>'1'
            
        ],[
            'Semester'=>'5',
            'Year'=>'3',
            'Group'=>'2'
            
        ],[
            'Semester'=>'6',
            'Year'=>'3',
            'Group'=>'1'
            
        ],[
            'Semester'=>'6',
            'Year'=>'3',
            'Group'=>'1'
            
        ],[
            'Semester'=>'7',
            'Year'=>'4',
            'Group'=>'1'
            
        ],[
            'Semester'=>'7',
            'Year'=>'4',
            'Group'=>'2'
            
        ]]);
    }
}
