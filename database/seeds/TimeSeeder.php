<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class TimeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('times')->insert([[
            'DayOfWeek'=>'Poniedziałek',
            'StartTime'=>'08:00:00',
            'EndTime'=>'12:00:00'
            
            
        ],[
            'DayOfWeek'=>'Wtorek',
            'StartTime'=>'08:00:00',
            'EndTime'=>'13:00:00'
            
            
        ],[
            'DayOfWeek'=>'Środa',
            'StartTime'=>'08:00:00',
            'EndTime'=>'14:00:00'
            
            
        ],[
            'DayOfWeek'=>'Czwartek',
            'StartTime'=>'08:00:00',
            'EndTime'=>'15:00:00'
            
            
        ],[
            'DayOfWeek'=>'Piątek',
            'StartTime'=>'08:00:00',
            'EndTime'=>'16:00:00'
            
            
        ],[
            'DayOfWeek'=>'Sobota',
            'StartTime'=>'08:00:00',
            'EndTime'=>'17:00:00'
            
            
        ],[
            'DayOfWeek'=>'Niedziela',
            'StartTime'=>'08:00:00',
            'EndTime'=>'18:00:00'
            
            
        ]]);
    }
}
