<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class ClassroomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('classrooms')->insert([[
            'ClassroomNo'=>'1',
            'Description'=>'Sala wykładowo-konwersatoryjna. Nie posiada projektora',
            'Capacity'=>'33',
            'Laboratory'=>'0',
            'Conversatory'=>'1',
            'Lecture'=>'1',
            'Seminar'=>'0'
            
        ],[
            'ClassroomNo'=>'2',
            'Description'=>'Sala wykładowo-konwersatoryjna. Nie posiada projektora',
            'Capacity'=>'33',
            'Laboratory'=>'0',
            'Conversatory'=>'1',
            'Lecture'=>'1',
            'Seminar'=>'0'
            
        ],[
            'ClassroomNo'=>'3',
            'Description'=>'Sala wykładowo-konwersatoryjna. Nie posiada projektora',
            'Capacity'=>'33',
            'Laboratory'=>'0',
            'Conversatory'=>'1',
            'Lecture'=>'1',
            'Seminar'=>'0'
            
        ],[
            'ClassroomNo'=>'104',
            'Description'=>'Sala laboratoryjno-seminaryjna. Posiada projektor. Posiada 16 stanowisk komputerowych',
            'Capacity'=>'26',
            'Laboratory'=>'1',
            'Conversatory'=>'0',
            'Lecture'=>'0',
            'Seminar'=>'1'
            
        ],[
            'ClassroomNo'=>'105',
            'Description'=>'Sala laboratoryjno-seminaryjna. Posiada projektor. Posiada 16 stanowisk komputerowych',
            'Capacity'=>'26',
            'Laboratory'=>'1',
            'Conversatory'=>'0',
            'Lecture'=>'0',
            'Seminar'=>'1'
            
        ],[
            'ClassroomNo'=>'215',
            'Description'=>'Sala laboratoryjna',
            'Capacity'=>'9',
            'Laboratory'=>'1',
            'Conversatory'=>'0',
            'Lecture'=>'0',
            'Seminar'=>'0'
            
        ],[
            'ClassroomNo'=>'110',
            'Description'=>'Sala wykładowa w Instytucie Informatyki. Posiada projektor',
            'Capacity'=>'63',
            'Laboratory'=>'0',
            'Conversatory'=>'0',
            'Lecture'=>'1',
            'Seminar'=>'1'
            
        ],[
            'ClassroomNo'=>'102',
            'Description'=>'Sala wykładowo-konwersatoryjna. Posiada projektor',
            'Capacity'=>'16',
            'Laboratory'=>'1',
            'Conversatory'=>'0',
            'Lecture'=>'0',
            'Seminar'=>'0'
            
        ],[
            'ClassroomNo'=>'104',
            'Description'=>'Sala laboratoryjna w Instytucie Informatyki. Posiada projektor',
            'Capacity'=>'16',
            'Laboratory'=>'1',
            'Conversatory'=>'0',
            'Lecture'=>'0',
            'Seminar'=>'0'
            
        ],[
            'ClassroomNo'=>'106',
            'Description'=>'Sala laboratoryjna w Instytucie Informatyki. Posiada projektor',
            'Capacity'=>'16',
            'Laboratory'=>'1',
            'Conversatory'=>'0',
            'Lecture'=>'0',
            'Seminar'=>'0'
            
        ],[
            'ClassroomNo'=>'108',
            'Description'=>'Sala laboratoryjna w Instytucie Informatyki. Posiada projektor',
            'Capacity'=>'17',
            'Laboratory'=>'1',
            'Conversatory'=>'0',
            'Lecture'=>'0',
            'Seminar'=>'0'
            
        ],[
            'ClassroomNo'=>'249',
            'Description'=>'Sala wykładowa wspólna z Instytutem Fizyki. Posiada projektor',
            'Capacity'=>'120',
            'Laboratory'=>'0',
            'Conversatory'=>'0',
            'Lecture'=>'1',
            'Seminar'=>'0'
            
        ]]);
    }
}
