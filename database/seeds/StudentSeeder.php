<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class StudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('students')->insert([[
            'Name'=>'Jan',
            'Surname'=>'Nowak',
            'Email'=>'j.nowak@gmail.com',
            'PhoneNumber'=>'123456789',
            'DateOfBirth'=>'1995-05-20',
            'Pesel'=>'95052079465',
            'IndexNumber'=>'123456',
            'IsActive'=>'1',
            'DegreeLevel'=>'1',
            'DegreeType'=>'inżynierskie',
        ],[
            'Name'=>'Jacek',
            'Surname'=>'Nowak',
            'Email'=>'j.nowak@gmail.com',
            'PhoneNumber'=>'123456789',
            'DateOfBirth'=>'1995-05-20',
            'Pesel'=>'95012079465',
            'IndexNumber'=>'123457',
            'IsActive'=>'1',
            'DegreeLevel'=>'1',
            'DegreeType'=>'inżynierskie',
        ],[
            'Name'=>'Adam',
            'Surname'=>'Kowalski',
            'Email'=>'j.nowak@gmail.com',
            'PhoneNumber'=>'123456789',
            'DateOfBirth'=>'1995-05-20',
            'Pesel'=>'95054079465',
            'IndexNumber'=>'123458',
            'IsActive'=>'1',
            'DegreeLevel'=>'1',
            'DegreeType'=>'licencjackie',
        ]]);
    }
}
