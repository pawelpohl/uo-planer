<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TeacherSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('teachers')->insert([[
            'Name'=>'Bartosz',
            'Surname'=>'Kowalski',
            'Email'=>'b.kow@gmail.com',
            'PhoneNumber'=>'987654321',
            'DateOfBirth'=>'1996-06-14',
            'Pesel'=>'96061413698',
            'TeacherId'=>'1',
            'Degree'=>'doktor',  
        ],[
            'Name'=>'Zbigniew',
            'Surname'=>'Malinowski',
            'Email'=>'z.mal@gmail.com',
            'PhoneNumber'=>'739146825',
            'DateOfBirth'=>'1970-10-19',
            'Pesel'=>'70101998745',
            'TeacherId'=>'2',
            'Degree'=>'profesor',
            
            
        ],[
            'Name'=>'Zbigniew',
            'Surname'=>'Adam',
            'Email'=>'z.mal@gmail.com',
            'PhoneNumber'=>'739146825',
            'DateOfBirth'=>'1970-10-19',
            'Pesel'=>'70101998445',
            'TeacherId'=>'3',
            'Degree'=>'doktor habilitowany',
        ]]);
    }
}
