<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('courses')->insert([[
            'CourseName'=>'Projekt zespołowy',
            'Description'=>'Przedmiot kierunkowy obowiązkowy',
            'LabHoursPerWeek'=>'5',
            'ConvHoursPerWeek'=>'0',
            'LecHoursPerWeek'=>'0',
            'SemHoursPerWeek'=>'0',
            'TotalHours'=>'75'
            
        ],[
            'CourseName'=>' Zagadnienia społeczne i zawodowe informatyki',
            'Description'=>'Przedmiot obowiązkowy',
            'LabHoursPerWeek'=>'0',
            'ConvHoursPerWeek'=>'1',
            'LecHoursPerWeek'=>'0',
            'SemHoursPerWeek'=>'0',
            'TotalHours'=>'15'
            
        ],[
            'CourseName'=>'Grafika komputerowa czasu rzeczywistego 1',
            'Description'=>'Przedmiot kierunkowy do wyboru',
            'LabHoursPerWeek'=>'3',
            'ConvHoursPerWeek'=>'0',
            'LecHoursPerWeek'=>'1',
            'SemHoursPerWeek'=>'0',
            'TotalHours'=>'60'
        ]]);
    }
}
