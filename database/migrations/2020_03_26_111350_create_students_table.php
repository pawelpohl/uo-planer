<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('Name');
            $table->text('Surname');
            $table->text('Email');
            $table->integer('PhoneNumber');
            $table->date('DateOfBirth');
            $table->bigInteger('Pesel');            
            $table->integer('IndexNumber');
            $table->boolean('IsActive');
            $table->integer('DegreeLevel');
            $table->text('DegreeType');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
