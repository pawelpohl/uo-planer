<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->id();
            $table->text('CourseName');
            $table->text('Description');
            $table->integer('LabHoursPerWeek');
            $table->integer('ConvHoursPerWeek');
            $table->integer('LecHoursPerWeek');
            $table->integer('SemHoursPerWeek');
            $table->integer('TotalHours');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
